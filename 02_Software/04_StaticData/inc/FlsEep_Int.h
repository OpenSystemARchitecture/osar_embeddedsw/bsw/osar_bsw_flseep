/*****************************************************************************************************************************
 * @file        FlsEep_Int.h                                                                                                 *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        19.08.2018 08:29:23                                                                                          *
 * @brief       Implementation of module global datatypes from the "FlsEep" module.                                          *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
 * @libNote     OSAR Source File Library v.1.0.0.0                                                                           *
*****************************************************************************************************************************/
#ifndef __FLSEEP_INT_H
#define __FLSEEP_INT_H

/**
 * @addtogroup OSAR_BSW 
 * @{
 */
/**
 * @addtogroup FlsEep
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 Start of include area                        << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#include "FlsEep_Types.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                  End of include area                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of public data types                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of public data types                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            Start of public definition area                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/* Page status definitions */
#define FLSEEP_ERASED                ((uint16)0xFFFF)     /* Page is empty */
#define FLSEEP_VALID_PAGE            ((uint16)0x0000)     /* Page containing valid data */

#define FLSEE_ERASED_FLS_VALUE       ((uint8)0xFF)        /* Fls erased memory value */
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of public definition area                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of global variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of global variables                      << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>           Start of public function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/**
 * @brief           Module initialization function.
 * @param[in]       None
 * @retval          None
 * @details         This function shall be called before using the module.
 */
void FlsEep_Int_Init( void );

/**
 * @brief           Processing function for the FlsEep Init statemachine
 * @param[in]       None
 * @retval          FlsEep_ReturnType
 *                  > FLSEEP_E_OK
 *                  > FLSEEP_E_NOT_OK
 *                  > FLSEEP_E_PENDING
 *                  > FLSEEP_E_REPEAT_CALL
 */
FlsEep_ReturnType FlsEep_Int_ProcessInitSm( void );

/**
 * @brief           Processing all request in active request list
 * @param[in]       None
 * @retval          FlsEep_ReturnType
 *                  > FLSEEP_E_OK         >> Request performed 
 *                  > FLSEEP_E_SKIPPED    >> Request skipped >> Not enough memory free >> Refactor flash memory
 */
FlsEep_ReturnType FlsEep_Int_ProcessRequests( void );

/**
 * @brief           Processing function to refactor the flash pages
 * @param[in]       None
 * @retval          None
 */
void FlsEep_Int_ProcessRefactorFlsPages( void );

/**
 * @brief           Search in configured blocks for the actual block id and return the configuration index
 * @param[in]       uint32 >> Block Id which shall be validated
 * @param[out]      uint16 >> index of block id in block config buffer 
 * @retval          FlsEep_ReturnType
 *                  > FLSEEP_E_OK         >> Block found 
 *                  > FLSEEP_E_NOT_OK     >> Block not found 
 */
FlsEep_ReturnType FlsEep_Int_SearchCfgIndexFromBlkId( uint32 blkId, uint16 *cfgIdx );

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of public function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/**
 * @}
 */
/**
 * @}
 */

#endif /* __FLSEEP_INT_H*/
