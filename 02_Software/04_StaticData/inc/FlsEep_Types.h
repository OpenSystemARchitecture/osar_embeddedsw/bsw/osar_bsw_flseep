/*****************************************************************************************************************************
 * @file        FlsEep_Types.h                                                                                               *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        19.08.2018 08:29:23                                                                                          *
 * @brief       Implementation of module global datatypes from the "FlsEep" module.                                          *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
 * @libNote     OSAR Source File Library v.1.0.0.0                                                                           *
*****************************************************************************************************************************/
#ifndef __FLSEEP_TYPES_H
#define __FLSEEP_TYPES_H

/**
 * @addtogroup OSAR_BSW 
 * @{
 */
/**
 * @addtogroup FlsEep
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 Start of include area                        << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#include "Std_Types.h"
#include "FlsEep_PBCfg.h"


/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                  End of include area                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of public data types                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/**
 * @brief           Available application errors of the FlsEep
 * @details         This enumeration implements all available application return and error values within this specific 
 *                  module. The Application error could be used as DET information or as return type of internal functions. 
 */
typedef enum{
  FLSEEP_E_OK = 0,
  FLSEEP_E_NOT_OK = 1,
  FLSEEP_E_PENDING = 2,
  FLSEEP_E_NOT_IMPLEMENTED = 3,
  FLSEEP_E_GENERIC_PROGRAMMING_FAILURE = 4,
  FLSEEP_E_REPEAT_CALL = 5,
  FLSEEP_E_INVALIDATE = 6,
  FLSEEP_E_SKIPPED = 7,

  FLSEEP_E_INVALID_ARGUMENT = 100,
  FLSEEP_E_NULL_POINTER = 101,
  FLSEEP_E_INVALID_CONDITION = 102

}FlsEep_ReturnType;

/**
 * @brief           Available application return values of the FlsEep
 * @details         Redefinition of the FlsEep_ReturnType as error type.
 */
typedef FlsEep_ReturnType FlsEep_ErrorType;

/**
 * @brief           FlsEep block configuration type
 */
typedef struct {
  uint32  flsEepBlkId;
  uint16  flsEepBlkLength;
}FlsEep_BlkCfgType;


/**
 * @brief           FlsEep Management data block look up table entry type
 */
typedef struct {
  uint32  flsEepDataBlkId;
  uint32  flsEepDataBlkAddress;
}FlsEep_MgmtDataBlkLutEntryType;

/**
 * @brief           FlsEep Management block type which would be stored on Flash
 */
typedef struct {
  FlsEep_MgmtDataBlkLutEntryType  blkAddressList[FLSEEP_CNT_OF_CONFIGURED_FLS_BLOCKS];
  uint32  mgmtBlkEntries;
  uint32  mgmtBlkCfgChecksum;
  uint32  mgmtBlkCrc;
}FlsEep_ManagementBlkType;

/**
 * @brief           FlsEep User Request Type
 */
typedef enum{
  FLSEEP_REQUEST_WRITE = 0,
  FLSEEP_REQUEST_READ = 1
}FlsEep_RequestType;

/**
 * @brief           FlsEep page configuration data
 */
typedef struct {
  uint32  pageStartAddress;
  uint16  pageFlsSectorId;
  uint32  pageLength;
}FlsEep_PageCfgType;

/**
 * @brief           FlsEep User Request List entry Type
 */
typedef struct
{
  uint16 blkCfgIdx;
  uint8* pBlkRamDataAddress;
  FlsEep_RequestType requestType;
  FlsEep_ReturnType blkStatus;
}FlsEep_RequestListEntryType;

/**
 * @brief           FlsEep internal init statemachine state type
 */
typedef enum{
  FLSEEP_INITSM_CHECK_PAGE_STATUS = 0,
  FLSEEP_INITSM_RESTORE_MGMTBLOCK,
  FLSEEP_INITSM_FORMAT_FLASH,
  FLSEEP_INITSM_WRITE_DEFAULT_MGMTBLK
}FlsEep_InitSm_StateType;

/**
 * @brief           FlsEep main statemachine state type
 */
typedef enum{
  FLSEEP_MAINSM_INIT = 0,
  FLSEEP_MAINSM_READY_FOR_OPERATION,
  FLSEEP_MAINSM_PROCESS_REQUEST,
  FLSEEP_MAINSM_REFACTOR_FLSPAGES,
  FLSEEP_MAINSM_SAFE_STATE
}FlsEep_MainSm_StateType;

/**
 * @brief           FlsEep active flash page memory management
 */
typedef struct {
  uint8 flsEep_Mgmt_ActivePageCfgId;
  uint32 flsEep_Mgmt_ActiveFlsAddress;
  uint32 flsEep_Mgmt_FlsPageMemoryFree;
}FlsEep_RuntimeFlsPageMgmtType;
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of public data types                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            Start of public definition area                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of public definition area                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of global variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of global variables                      << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>           Start of public function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of public function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/**
 * @}
 */
/**
 * @}
 */

#endif /* __FLSEEP_TYPES_H*/
