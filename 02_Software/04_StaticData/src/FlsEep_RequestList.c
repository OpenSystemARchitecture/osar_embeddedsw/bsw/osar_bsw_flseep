/*****************************************************************************************************************************
 * @file        FlsEep_RequestList.c                                                                                      *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        19.08.2018 08:29:23                                                                                          *
 * @brief       Implementation of functionalities from the "FlsEep" module.                                                  *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
 * @libNote     OSAR Source File Library v.1.0.0.0                                                                           *
*****************************************************************************************************************************/

/**
 * @addtogroup OSAR_BSW 
 * @{
 */
/**
 * @addtogroup FlsEep
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 Start of include area                        << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#include "Det.h"
#include "FlsEep_RequestList.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                  End of include area                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of private data types                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of private data types                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            Start of private definitions area                 << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of private definitions area                  << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of global variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/*------------------------------------------- Map variables into constand memory -------------------------------------------*/
#define FlsEep_START_SEC_CONST
#include "FlsEep_MemMap.h"
#define FlsEep_STOP_SEC_CONST
#include "FlsEep_MemMap.h"
/*---------------------------------------- Map variables into uninitialized memory ----------------------------------------*/
#define FlsEep_START_SEC_NOINIT_VAR
#include "FlsEep_MemMap.h"
#define FlsEep_STOP_SEC_NOINIT_VAR
#include "FlsEep_MemMap.h"
/*----------------------------------------- Map variables into initialized memory -----------------------------------------*/
#define FlsEep_START_SEC_INIT_VAR
#include "FlsEep_MemMap.h"
#define FlsEep_STOP_SEC_INIT_VAR
#include "FlsEep_MemMap.h"
/*--------------------------------------- Map variables into zero initialized memory ---------------------------------------*/
#define FlsEep_START_SEC_ZERO_INIT_VAR
#include "FlsEep_MemMap.h"
#define FlsEep_STOP_SEC_ZERO_INIT_VAR
#include "FlsEep_MemMap.h"

/*------------------------------------------------ Extern defined data -----------------------------------------------------*/
extern const FlsEep_BlkCfgType flsEepConfiguredBlocks[];
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of global variables                      << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of private variables                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/*------------------------------------------- Map variables into constand memory -------------------------------------------*/
#define FlsEep_START_SEC_CONST
#include "FlsEep_MemMap.h"
#define FlsEep_STOP_SEC_CONST
#include "FlsEep_MemMap.h"
/*---------------------------------------- Map variables into uninitialized memory ----------------------------------------*/
#define FlsEep_START_SEC_NOINIT_VAR
#include "FlsEep_MemMap.h"
FlsEep_RequestListEntryType flsEep_RequestList_List[FLSEEP_CNT_OF_MAX_REQUESTS_PER_CYCLE];
uint16 flsEep_RequestList_ActiveElementIndex;
uint16 flsEep_RequestList_NextElementIndex;
#define FlsEep_STOP_SEC_NOINIT_VAR
#include "FlsEep_MemMap.h"
/*----------------------------------------- Map variables into initialized memory -----------------------------------------*/
#define FlsEep_START_SEC_INIT_VAR
#include "FlsEep_MemMap.h"
#define FlsEep_STOP_SEC_INIT_VAR
#include "FlsEep_MemMap.h"
/*--------------------------------------- Map variables into zero initialized memory ---------------------------------------*/
#define FlsEep_START_SEC_ZERO_INIT_VAR
#include "FlsEep_MemMap.h"
#define FlsEep_STOP_SEC_ZERO_INIT_VAR
#include "FlsEep_MemMap.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of private variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>           Start of private function prototypes               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            End of private function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>        Start of private function implementation              << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/*--------------------------------------------- Map function into code memory ---------------------------------------------*/
#define FlsEep_START_SEC_CODE
#include "FlsEep_MemMap.h"
#define FlsEep_STOP_SEC_CODE
#include "FlsEep_MemMap.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>         End of private function implementation               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>          Start of public function implementation             << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#define FlsEep_START_SEC_CODE
#include "FlsEep_MemMap.h"
/**
 * @brief           Module initialization function.
 * @param[in]       None
 * @retval          None
 * @details         This function shall be called before using the module
 */
void FlsEep_RequestList_Init(void)
{
  uint16 idx;

  /* Initialize the request list */
  for(idx = 0; idx < FLSEEP_CNT_OF_MAX_REQUESTS_PER_CYCLE; idx++)
  {
    flsEep_RequestList_List[idx].blkCfgIdx = 0xFFFF;
    flsEep_RequestList_List[idx].blkStatus = FLSEEP_E_NOT_OK;
    flsEep_RequestList_List[idx].pBlkRamDataAddress = NULL_PTR;
    flsEep_RequestList_List[idx].requestType = FLSEEP_REQUEST_READ;
  }

  flsEep_RequestList_ActiveElementIndex = 0;
  flsEep_RequestList_NextElementIndex = 0;
}

/**
 * @brief           Add a new request entry to user request list
 * @param[in]       uint16 FlsEep block config index
 * @param[in]       uint8* pointer to ram block data
 * @param[in]       FlsEep_RequestType request type
 * @retval          FlsEep_ReturnType
 *                  > FLSEEP_E_OK            >> Request accepted
 *                  > FLSEEP_E_NOT_OK        >> Request not accepted
 */
FlsEep_ReturnType FlsEep_RequestList_AddNewRequest( uint16 blkCfgIdx, uint8* pBlkRamDataAddress, FlsEep_RequestType requestType )
{
  FlsEep_ReturnType retVal;
  uint16 idx;
  boolean skippRequestBecauseElementIsProcessing = FALSE;
  /* Check input parameter */
  if(NULL_PTR == pBlkRamDataAddress)
  {
    #if(STD_ON == FLSEEP_MODULE_USE_DET)
    Det_ReportError(FLSEEP_DET_MODULE_ID, FLSEEP_E_NULL_POINTER);
    #endif
    retVal = FLSEEP_E_NOT_OK;
  }
  else if(blkCfgIdx > (FLSEEP_CNT_OF_CONFIGURED_FLS_BLOCKS - 1))
  {
    #if(STD_ON == FLSEEP_MODULE_USE_DET)
    Det_ReportError(FLSEEP_DET_MODULE_ID, FLSEEP_E_INVALID_ARGUMENT);
    #endif
    retVal = FLSEEP_E_NOT_OK;
  }
  else if((FLSEEP_CNT_OF_MAX_REQUESTS_PER_CYCLE - 1) == FlsEep_RequestList_GetRequestLoad())
  {
    /* List is full >> Do not accept another request */
    #if(STD_ON == FLSEEP_MODULE_USE_DET)
    Det_ReportError(FLSEEP_DET_MODULE_ID, FLSEEP_E_INVALID_CONDITION);
    #endif
    retVal = FLSEEP_E_SKIPPED;
  }
  else
  {
    /* Search for old Request and delete ist if it is not pending any more */
    for(idx = 0; idx < FLSEEP_CNT_OF_MAX_REQUESTS_PER_CYCLE; idx++)
    {
      /* Search for current request element in request list */
      if(flsEep_RequestList_List[idx].blkCfgIdx == blkCfgIdx )
      {
        /* Element found >> Check now if it is still in progress */
        if(FLSEEP_E_PENDING == flsEep_RequestList_List[idx].blkStatus )
        {
          /* Element found and still processing >> Skipp new Request */
          skippRequestBecauseElementIsProcessing == TRUE;
          retVal = FLSEEP_E_SKIPPED;
        }
        else
        {
          /* Element found and processing finished >> Cleanup olf element and continue with request processing */
          flsEep_RequestList_List[idx].blkCfgIdx = 0xFFFF;
          flsEep_RequestList_List[idx].blkStatus = FLSEEP_E_NOT_OK;
          flsEep_RequestList_List[idx].pBlkRamDataAddress = NULL_PTR;
          flsEep_RequestList_List[idx].requestType = FLSEEP_REQUEST_READ;
        }
      }
    }

    /* Check if element is still processing */
    if(FALSE == skippRequestBecauseElementIsProcessing)
    {
      /* Copy Data */
      flsEep_RequestList_List[flsEep_RequestList_NextElementIndex].blkCfgIdx = blkCfgIdx;
      flsEep_RequestList_List[flsEep_RequestList_NextElementIndex].blkStatus = FLSEEP_E_PENDING;
      flsEep_RequestList_List[flsEep_RequestList_NextElementIndex].pBlkRamDataAddress = pBlkRamDataAddress;
      flsEep_RequestList_List[flsEep_RequestList_NextElementIndex].requestType = requestType;

      /* Update List Pointer */
      flsEep_RequestList_NextElementIndex++;
      if(flsEep_RequestList_NextElementIndex >= FLSEEP_CNT_OF_MAX_REQUESTS_PER_CYCLE)
      {
        flsEep_RequestList_NextElementIndex = 0;
      }

      retVal = FLSEEP_E_OK;
    }
  }

  return retVal;
}

/**
 * @brief           Get the pointer to the active request element
 * @param[out]      const FlsEep_RequestListEntryType* pointer to active element
 * @retval          FlsEep_ReturnType
 *                  > FLSEEP_E_OK            >> Request accepted
 *                  > FLSEEP_E_NOT_OK        >> Request not accepted
 */
FlsEep_ReturnType FlsEep_RequestList_GetActiveRequest( FlsEep_RequestListEntryType* activeRequestElement )
{
  FlsEep_ReturnType retVal;

  /* Check input parameter */
  if(NULL_PTR == activeRequestElement)
  {
    #if(STD_ON == FLSEEP_MODULE_USE_DET)
    Det_ReportError(FLSEEP_DET_MODULE_ID, FLSEEP_E_NULL_POINTER);
    #endif
    retVal = FLSEEP_E_NOT_OK;
  }
  else
  {
    if(0 == FlsEep_RequestList_GetRequestLoad())
    {
      #if(STD_ON == FLSEEP_MODULE_USE_DET)
      Det_ReportError(FLSEEP_DET_MODULE_ID, FLSEEP_E_INVALID_CONDITION);
      #endif
      retVal = FLSEEP_E_NOT_OK;
    }
    else
    {
      /* Copy active request data */
      activeRequestElement->blkCfgIdx = flsEep_RequestList_List[flsEep_RequestList_ActiveElementIndex].blkCfgIdx;
      activeRequestElement->blkStatus = flsEep_RequestList_List[flsEep_RequestList_ActiveElementIndex].blkStatus;
      activeRequestElement->pBlkRamDataAddress = flsEep_RequestList_List[flsEep_RequestList_ActiveElementIndex].pBlkRamDataAddress;
      activeRequestElement->requestType = flsEep_RequestList_List[flsEep_RequestList_ActiveElementIndex].requestType;
      retVal = FLSEEP_E_OK;
    }

  }
  return retVal;
}

/**
 * @brief           Get the status of a requested block
 * @param[in]       uint16 FlsEep block config index
 * @retval          FlsEep_ReturnType
 *                  > FLSEEP_E_OK            >> Request processed correctly 
 *                  > FLSEEP_E_NOT_OK        >> Request processing failed
 *                  > FLSEEP_E_PENDING       >> Request still in progress 
 *                  > FLSEEP_E_INVALIDATE    >> Request skipped because of invalidate flash data
 * @note            Is requested is is not in list, the status would be reported as ok.
 */
FlsEep_ReturnType FlsEep_RequestList_GetRequestStatus( uint16 blkCfgIdx )
{
  FlsEep_ReturnType retVal = FLSEEP_E_OK;
  uint8 idx;

  for(idx = 0; idx < FLSEEP_CNT_OF_MAX_REQUESTS_PER_CYCLE; idx++ )
  {
    if( flsEep_RequestList_List[idx].blkCfgIdx == blkCfgIdx ) 
    {
      retVal = flsEep_RequestList_List[idx].blkStatus;
      break;
    }
  }

  return retVal;
}

/**
 * @brief           Get the status of the active requested block
 * @param[in]       None
 * @retval          FlsEep_ReturnType
 *                  > FLSEEP_E_OK            >> Request processed correctly 
 *                  > FLSEEP_E_NOT_OK        >> Request processing failed
 *                  > FLSEEP_E_PENDING       >> Request still in progres 
 *                  > FLSEEP_E_INVALIDATE    >> Request skipped because of invalidate flash data
 */
FlsEep_ReturnType FlsEep_RequestList_GetActiveRequestStatus( void )
{
  return flsEep_RequestList_List[flsEep_RequestList_ActiveElementIndex].blkStatus;
}

/**
 * @brief           Get the active request type
 * @param[in]       None
 * @retval          FlsEep_RequestType
 *                  > FLSEEP_REQUEST_WRITE       >> Request write
 *                  > FLSEEP_REQUEST_READ        >> Request read
 */
FlsEep_RequestType FlsEep_RequestList_GetActiveRequestType( void )
{
  return flsEep_RequestList_List[flsEep_RequestList_ActiveElementIndex].requestType;
}

/**
 * @brief           Set a new request processing status
 * @param[in]       FlsEep_ReturnType new processing status
 *                  > FLSEEP_E_OK
 *                  > FLSEEP_E_NOT_OK
 *                  > FLSEEP_E_PENDING
 *                  > FLSEEP_E_INVALIDATE
 * @retval          FlsEep_ReturnType
 *                  > FLSEEP_E_OK            >> Request accepted
 *                  > FLSEEP_E_NOT_OK        >> Request not accepted
 * @note            If the new status is OK / Not_Ok / Skipped the system would change the active status element.
 */
FlsEep_ReturnType FlsEep_RequestList_SetActiveRequestStatus( FlsEep_ReturnType requestProcessingStatus )
{
  FlsEep_ReturnType retVal;
  /* Check input parameter */
  if( !( (FLSEEP_E_OK == requestProcessingStatus ) || (FLSEEP_E_NOT_OK == requestProcessingStatus ) ||
       (FLSEEP_E_PENDING == requestProcessingStatus ) || (FLSEEP_E_INVALIDATE == requestProcessingStatus )))
  {
    #if(STD_ON == FLSEEP_MODULE_USE_DET)
    Det_ReportError(FLSEEP_DET_MODULE_ID, FLSEEP_E_INVALID_ARGUMENT);
    #endif
    retVal = FLSEEP_E_NOT_OK;
  }
  else
  {

    /* Set status */
    flsEep_RequestList_List[flsEep_RequestList_ActiveElementIndex].blkStatus = requestProcessingStatus;


    /* Check if there is a next active element */
    if(0 != FlsEep_RequestList_GetRequestLoad() )
    {
      if( (FLSEEP_E_OK == requestProcessingStatus ) || (FLSEEP_E_NOT_OK == requestProcessingStatus )|| (FLSEEP_E_INVALIDATE == requestProcessingStatus ) )
      {
        /* Change Active Element*/
        flsEep_RequestList_ActiveElementIndex++;

        if(flsEep_RequestList_ActiveElementIndex >= FLSEEP_CNT_OF_MAX_REQUESTS_PER_CYCLE)
        {
          flsEep_RequestList_ActiveElementIndex = 0;
        }
      }
    }

    retVal = FLSEEP_E_OK;

  }
  return retVal;
}

/**
 * @brief           Get a request list load
 * @param[in]       None
 * @retval          uint8 list load
 */
uint8 FlsEep_RequestList_GetRequestLoad( void )
{
  uint8 retVal;

  if(flsEep_RequestList_NextElementIndex == flsEep_RequestList_ActiveElementIndex)
  {
    /* List is empty */
    retVal = 0;
  }
  else if(flsEep_RequestList_NextElementIndex > flsEep_RequestList_ActiveElementIndex)
  {
    /* List as elements */
    retVal = flsEep_RequestList_NextElementIndex - flsEep_RequestList_ActiveElementIndex;
  }
  else // if(flsEep_RequestList_NextElementIndex < flsEep_RequestList_ActiveElementIndex)
  {
    /* List as elements */
    retVal =  (FLSEEP_CNT_OF_MAX_REQUESTS_PER_CYCLE - flsEep_RequestList_ActiveElementIndex) + flsEep_RequestList_NextElementIndex;
  }
  return retVal;
}

/**
 * @brief           Get complete write data load of all pending list elements 
 * @param[in]       None
 * @retval          uint32 list data load
 */
uint32 FlsEep_RequestList_GetCompleteListWriteDataLoad(void)
{
  boolean breakCalculation = FALSE;
  uint8 lstIdx;
  uint32 dataLoad = 0;

  lstIdx = flsEep_RequestList_ActiveElementIndex;
  while(FALSE == breakCalculation)
  {

    if(FLSEEP_E_PENDING == flsEep_RequestList_List[lstIdx].blkStatus)
    {
      /* Accumulate write data load */
      if(FLSEEP_REQUEST_WRITE == flsEep_RequestList_List[lstIdx].requestType)
      {
        dataLoad += flsEepConfiguredBlocks[flsEep_RequestList_List[lstIdx].blkCfgIdx].flsEepBlkLength;
      }
      
      /* Increment processing index */
      lstIdx++;
      if(lstIdx >= FLSEEP_CNT_OF_MAX_REQUESTS_PER_CYCLE)
      {
        lstIdx = 0;
      }

      if(lstIdx == flsEep_RequestList_NextElementIndex)
      {
        breakCalculation = TRUE;
      }

    }
    else
    {
      breakCalculation = TRUE;
    }
  }

  return dataLoad;
}
#define FlsEep_STOP_SEC_CODE
#include "FlsEep_MemMap.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>          End of public function implementation               << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/**
 * @}
 */
/**
 * @}
 */
