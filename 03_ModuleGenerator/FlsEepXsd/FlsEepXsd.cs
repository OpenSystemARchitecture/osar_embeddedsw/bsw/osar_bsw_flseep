﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OsarResources.XML;
using OsarResources.Generic;

namespace FlsEepXsd
{
  public struct FlsEepPageCfg
  {
    public String flsEepPageStartAddress;
    public String flsEepPageLength;
    public Byte flsEepFlashSectorId;
  }

  public struct FlsEepBlkCfg
  {
    public string flsEepBlkName;
    public UInt16 flsEepBlkLength;

  }

  public class FlsEepXml
  {
    public XmlFileVersion xmlFileVersion;
    public UInt16 detModuleID;
    public SystemState detModuleUsage;

    public List<FlsEepPageCfg> flsEepPageCfgList;
    public List<FlsEepBlkCfg> flsEepBlkCfgList;
    public Byte flsEepRequestQueue;

    public UInt16 miBMainfunctionCycleTimeMs;
  }
}
