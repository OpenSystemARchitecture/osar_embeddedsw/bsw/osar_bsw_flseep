﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ModuleLibrary;
using OsarResources.Generator;

namespace TestModuleLibrary
{
  /// <summary>
  /// Interaction logic for MainWindow.xaml
  /// </summary>
  public partial class MainWindow : Window
  {
    public MainWindow()
    {
      InitializeComponent();

      OsarModuleContextInterface osarModuleContext = ModuleContext.GetInstance();

      CC_moduleCfg.Content = ModuleContext.GetInstance().GetView(".\\..\\..\\Test\\01_Generator\\ModuleConfig.xml", ".\\..\\..\\Test\\");



    }

    private void B_Validate_Click(object sender, RoutedEventArgs e)
    {
      GenInfoType results = ModuleContext.GetInstance().GetViewModel(".\\..\\..\\Test\\01_Generator\\ModuleConfig.xml", ".\\..\\..\\Test\\").ValidateConfiguration();
    }

    private void B_Generate_Click(object sender, RoutedEventArgs e)
    {
      GenInfoType results = ModuleContext.GetInstance().GetViewModel(".\\..\\..\\Test\\01_Generator\\ModuleConfig.xml", ".\\..\\..\\Test\\").GenerateConfiguration();
    }
  }
}
