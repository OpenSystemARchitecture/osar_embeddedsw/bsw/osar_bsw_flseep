/*****************************************************************************************************************************
 * @file        FlsEep_PBCfg.h                                                                                               *
 * @author      OSAR S.Reinemuth                                                                                             *
 * @date        26.09.2019 09:52:23                                                                                          *
 * @brief       Generated header file data of the FlsEep module.                                                             *
 * @version     v.1.2.1                                                                                                      *
 * @generator   OSAR FlsEep Generator v.0.0.0.57                                                                             *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
 * @libNote     OSAR Source File Library v.1.1.7.1                                                                           *
*****************************************************************************************************************************/

#ifndef __FLSEEP_PBCFG_H
#define __FLSEEP_PBCFG_H

/**
 * @addtogroup OSAR_BSW 
 * @{
 */
/**
 * @addtogroup FlsEep 
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 Start of include area                        << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#include "Std_Types.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                  End of include area                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of public data types                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of public data types                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            Start of public definition area                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/*--------------------------------------------- FlsEep Module Det Information ---------------------------------------------*/
#define FLSEEP_DET_MODULE_ID                 7
#define FLSEEP_MODULE_USE_DET                STD_ON
/*----------------------------------------- FlsEep basic module configuration data -----------------------------------------*/
#define FLSEEP_USED_PAGES                    2
#define FLSEEP_CONFIGURATION_CHECKSUM        (uint32)0xE97CE799
#define FLSEEP_USED_PAGES                    2
#define FLSEEP_CNT_OF_MAX_REQUESTS_PER_CYCLE 10
#define FLSEEP_MGMT_BLK_ID                   (uint32)0x7AD08B7F
/*--------------------------------------- FlsEep user flash block configuration data ---------------------------------------*/
#define FLSEEP_BLK_DEMOBLK                   (uint32)0xA37CD013
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of public definition area                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of global variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of global variables                      << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>           Start of public function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of public function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/**
 * @}
 */
/**
 * @}
 */

#endif /* __FLSEEP_PBCFG_H*/
