﻿/*****************************************************************************************************************************
 * @file        ModuleValidator.cs                                                                                           *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        06.04.2019                                                                                                   *
 * @brief       Implementation of the Module Validation Class                                                                *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
* @addtogroup ModuleLibrary.Versions.v_1_0_0.Generator
* @{
*/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OsarResources.Generator;
using OsarResources.XML;
using OsarResources.Generator.Resources;
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace ModuleLibrary.Versions.v_1_0_0.Generator
{
  internal class ModuleValidator
  {
    private Models.FlsEepXml xmlCfg;
    private string pathToConfiguratioFile;
    private GenInfoType info;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="cfgFile"> Config file which shall be generated </param>
    /// <param name="pathToCfgFile"> Path of Configuration file </param>
    public ModuleValidator(Models.FlsEepXml cfgFile, string pathToCfgFile)
    {
      xmlCfg = cfgFile;
      pathToConfiguratioFile = pathToCfgFile;

      info.info = new List<string>();
      info.log = new List<string>();
      info.warning = new List<string>();
      info.error = new List<string>();
    }

    /// <summary>
    /// Interface to validate the configuration
    /// </summary>
    /// <returns> Generation information </returns>
    public GenInfoType ValidateConfiguration()
    {
      int idx, idx2;

      info.AddLogMsg(ValidateResources.LogMsg_StartOfValidation);

      /* Validate configuration file version */
      XmlFileVersion defaultVersion = new XmlFileVersion();
      defaultVersion.MajorVersion = Convert.ToUInt16(DefResources.CfgFileMajorVersion);
      defaultVersion.MinorVersion = Convert.ToUInt16(DefResources.CfgFileMinorVersion);
      defaultVersion.PatchVersion = Convert.ToUInt16(DefResources.CfgFilePatchVersion);
      if (false == OsarResources.Generic.OsarGenericHelper.XmlFileVersionEqual(defaultVersion, xmlCfg.xmlFileVersion))
      {
        info.AddErrorMsg(Convert.ToUInt16(ErrorWarningCodes.Error_0000), ErrorWarningCodes.Error_0000_Msg);
      }

      /* Check if configuration file parameter is available */
      /* Check if flash pages has been configured */
      info.AddLogMsg(ValidateResources.LogMsg_CheckFlashPages);
      if (xmlCfg.flsEepPageCfgList == null)
      {
        info.AddErrorMsg(Convert.ToUInt16(GenErrorWarningCodes.Error_1000), GenErrorWarningCodes.Error_1000_Msg);
      }
      if (xmlCfg.flsEepPageCfgList.Count != 2)
      {
        info.AddErrorMsg(Convert.ToUInt16(GenErrorWarningCodes.Error_1001), GenErrorWarningCodes.Error_1001_Msg);
      }

      /* Check if flash page configuration */
      if (!( ( System.Text.RegularExpressions.Regex.IsMatch(xmlCfg.flsEepPageCfgList[0].flsEepPageStartAddress, @"\A\b[0-9a-fA-Fx]+\b\Z") ) &&
                ( xmlCfg.flsEepPageCfgList[0].flsEepPageStartAddress.Length == 10 ) ))
      {
        info.AddErrorMsg(Convert.ToUInt16(GenErrorWarningCodes.Error_1002), GenErrorWarningCodes.Error_1002_Msg);
      }

      if (!( ( System.Text.RegularExpressions.Regex.IsMatch(xmlCfg.flsEepPageCfgList[0].flsEepPageLength, @"\A\b[0-9a-fA-Fx]+\b\Z") ) ))
      {
        info.AddErrorMsg(Convert.ToUInt16(GenErrorWarningCodes.Error_1003), GenErrorWarningCodes.Error_1003_Msg);
      }

      if (!( ( System.Text.RegularExpressions.Regex.IsMatch(xmlCfg.flsEepPageCfgList[1].flsEepPageStartAddress, @"\A\b[0-9a-fA-Fx]+\b\Z") ) &&
                ( xmlCfg.flsEepPageCfgList[1].flsEepPageStartAddress.Length == 10 ) ))
      {
        info.AddErrorMsg(Convert.ToUInt16(GenErrorWarningCodes.Error_1004), GenErrorWarningCodes.Error_1004_Msg);
      }

      if (!( ( System.Text.RegularExpressions.Regex.IsMatch(xmlCfg.flsEepPageCfgList[1].flsEepPageLength, @"\A\b[0-9a-fA-Fx]+\b\Z") ) ))
      {
        info.AddErrorMsg(Convert.ToUInt16(GenErrorWarningCodes.Error_1005), GenErrorWarningCodes.Error_1005_Msg);
      }

      /* Check for duplicated elements flash pages */
      if (( xmlCfg.flsEepPageCfgList[0].flsEepPageStartAddress == xmlCfg.flsEepPageCfgList[1].flsEepPageStartAddress ) ||
          ( xmlCfg.flsEepPageCfgList[0].flsEepFlashSectorId == xmlCfg.flsEepPageCfgList[1].flsEepFlashSectorId ))
      {
        info.AddErrorMsg(Convert.ToUInt16(GenErrorWarningCodes.Error_1006), GenErrorWarningCodes.Error_1006_Msg);
      }

      /* Check if flash block has been configured */
      info.AddLogMsg(ValidateResources.LogMsg_CheckFlashBlocks);
      if (xmlCfg.flsEepBlkCfgList == null)
      {
        info.AddErrorMsg(Convert.ToUInt16(GenErrorWarningCodes.Error_1007), GenErrorWarningCodes.Error_1007_Msg);
      }


      /* Check for duplicated flash block names */
      for (idx = 0; idx < xmlCfg.flsEepBlkCfgList.Count; idx++)
      {
        /* Check for duplicate entries */
        for (idx2 = 0; idx2 < xmlCfg.flsEepBlkCfgList.Count; idx2++)
        {
          /* Skipp active configuration */
          if (idx != idx2)
          {
            if (xmlCfg.flsEepBlkCfgList[idx].flsEepBlkName == xmlCfg.flsEepBlkCfgList[idx2].flsEepBlkName)
            {
              info.AddErrorMsg(Convert.ToUInt16(GenErrorWarningCodes.Error_1008), GenErrorWarningCodes.Error_1008_Msg + " >> Config Index 1:" + idx.ToString() + " >> Config Index 2:" + idx2.ToString());
            }
          }
        }

        /* Check if name contains spaces */
        if (xmlCfg.flsEepBlkCfgList[idx].flsEepBlkName.Contains(" "))
        {
          info.AddErrorMsg(Convert.ToUInt16(GenErrorWarningCodes.Error_1009), GenErrorWarningCodes.Error_1009_Msg + " >> Name: " + xmlCfg.flsEepBlkCfgList[idx].flsEepBlkName);
        }
      }

      /* Check for minimum buffer length */
      info.AddLogMsg(ValidateResources.LogMsg_CheckGenericConfigData);
      if (xmlCfg.flsEepRequestQueue < 2)
      {
        info.AddErrorMsg(Convert.ToUInt16(GenErrorWarningCodes.Error_1010), GenErrorWarningCodes.Error_1010_Msg);
      }

      /* Check for mainfunction cycle time */
      if ( 0 == xmlCfg.miBMainfunctionCycleTimeMs)
      {
        info.AddErrorMsg(Convert.ToUInt16(GenErrorWarningCodes.Error_1011), GenErrorWarningCodes.Error_1011_Msg);
      }

      info.AddLogMsg(ValidateResources.LogMsg_ValidationDone);
      return info;
    }
  }
}
