﻿/*****************************************************************************************************************************
 * @file        DefaultCfg.cs                                                                                                *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        06.04.2019                                                                                                   *
 * @brief       Implementation of the Default Module Class                                                                   *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
* @addtogroup ModuleLibrary.Versions.v_1_0_0.Generator
* @{
*/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModuleLibrary.Versions.v_1_0_0.Models;
using OsarResources.Generator;
using OsarResources.Generic;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace ModuleLibrary.Versions.v_1_0_0.Generator
{
  internal class DefaultCfgGenerator
  {
    private Models.FlsEepXml xmlCfg;
    private string pathToConfiguratioFile;
    private GenInfoType info;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="cfgFile"> Config file which shall be set to default </param>
    /// <param name="pathToCfgFile"> Path of Configuration file </param>
    public DefaultCfgGenerator(Models.FlsEepXml cfgFile, string pathToCfgFile)
    {
      xmlCfg = cfgFile;
      pathToConfiguratioFile = pathToCfgFile;

      info.info = new List<string>();
      info.log = new List<string>();
      info.warning = new List<string>();
      info.error = new List<string>();
    }

    /// <summary>
    /// Interface to set default configuration
    /// </summary>
    /// <returns> Generation information </returns>
    public GenInfoType CreateDefaultConfiguration()
    {
      info.AddInfoMsg(DefResources.InfoMsg_StartDefaultCfgCreation + DefResources.CfgFileMajorVersion +
        "." + DefResources.CfgFileMinorVersion + "." + DefResources.CfgFilePatchVersion);
      info.AddLogMsg(DefResources.LogMsg_StartDefaultCfgCreation);

      /* Create default configuration*/
      if (null == xmlCfg)
      {
        xmlCfg = new Models.FlsEepXml();
      }

      // Create Configuration File Version
      xmlCfg.xmlFileVersion.MajorVersion = Convert.ToUInt16(DefResources.CfgFileMajorVersion);
      xmlCfg.xmlFileVersion.MinorVersion = Convert.ToUInt16(DefResources.CfgFileMinorVersion);
      xmlCfg.xmlFileVersion.PatchVersion = Convert.ToUInt16(DefResources.CfgFilePatchVersion);

      // Create Det Module Id
      xmlCfg.detModuleID = Convert.ToUInt16(DefResources.DefaultModuleId);

      // Create module Det usage state
      if (DefResources.DefaultModuleUsage == SystemState.STD_ON.ToString())
        xmlCfg.detModuleUsage = SystemState.STD_ON;
      else
        xmlCfg.detModuleUsage = SystemState.STD_OFF;

      // Set Data default flash page configuration
      xmlCfg.flsEepPageCfgList = new List<FlsEepPageCfg>();
      //xmlCfg.flsEepPageCfgList = new FlsEepPageCfg[2];
      FlsEepPageCfg pageCfg = new FlsEepPageCfg();
      pageCfg = new FlsEepPageCfg();
      pageCfg.flsEepFlashSectorId = 2;
      pageCfg.flsEepPageLength = "0x4000";
      pageCfg.flsEepPageStartAddress = "0x08008000";
      xmlCfg.flsEepPageCfgList.Add(pageCfg);

      pageCfg = new FlsEepPageCfg();
      pageCfg.flsEepFlashSectorId = 3;
      pageCfg.flsEepPageLength = "0x4000";
      pageCfg.flsEepPageStartAddress = "0x0800C000";
      xmlCfg.flsEepPageCfgList.Add(pageCfg);

      /* Set Data default flash block configuration */
      xmlCfg.flsEepBlkCfgList = new List<FlsEepBlkCfg>();
      FlsEepBlkCfg blockCfg = new FlsEepBlkCfg();
      blockCfg.flsEepBlkLength = 14;
      blockCfg.flsEepBlkName = "DemoBlk";
      xmlCfg.flsEepBlkCfgList.Add(blockCfg);

      /* Set default Request Queue length */
      xmlCfg.flsEepRequestQueue = 10;

      /* Set default mainfunction cycle time length */
      xmlCfg.miBMainfunctionCycleTimeMs = 10;

      info.AddLogMsg(DefResources.LogMsg_CreationDone);

      return info;
    }
  }
}
