﻿using ModuleLibrary.Versions.v_1_0_0.Models;
using ModuleLibrary.Versions.v_1_0_0.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ModuleLibrary.Versions.v_1_0_0.Views
{
  /// <summary>
  /// Interaction logic for the ModuleCfgView.xaml
  /// </summary>
  public partial class ModuleCfgView : UserControl
  {
    Module_ViewModel newCfg;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="pathToCfgFile">Absolute Path to configuration file</param>
    /// <param name="absPathToModuleBaseFolder">Absolute Path to module base folder</param>
    public ModuleCfgView(string pathToCfgFile, string absPathToModuleBaseFolder)
    {
      newCfg = new Module_ViewModel(pathToCfgFile, absPathToModuleBaseFolder);

      InitializeComponent();

      /* Set Data Context where data is binded to */
      this.DataContext = newCfg;

    }

    /// <summary>
    /// Button to add a new block to block config structure
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void B_AddNewBlock_Click(object sender, RoutedEventArgs e)
    {
      newCfg.AddNewBlockEntry();
    }

    /// <summary>
    /// Button to remove the selected block
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void B_RemoveSelectedBlock_Click(object sender, RoutedEventArgs e)
    {
      BlockCfgVM removedBlock = (BlockCfgVM)DG_BlockConfig.SelectedItem;
      newCfg.RemoveBlockCfg(removedBlock);
    }
  }
}
