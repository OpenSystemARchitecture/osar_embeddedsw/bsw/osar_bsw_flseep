﻿/*****************************************************************************************************************************
 * @file        BlockCfgVM.cs                                                                                                *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        26.03.2019                                                                                                   *
 * @brief       Implementation of the Block Configuration View Model                                                         *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
 * @addtogroup ModuleLibrary.Versions.v_1_0_0.ViewModels
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModuleLibrary.Versions.v_1_0_0.Models;
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name Space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace ModuleLibrary.Versions.v_1_0_0.ViewModels
{
  /// <summary>
  /// Data Model for the Block Configuration Data
  /// </summary>
  public class BlockCfgVM : BaseViewModel
  {
    FlsEepBlkCfg blockConfig;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="blockCfg"></param>
    /// <param name="storeCfgDel"></param>
    public BlockCfgVM(FlsEepBlkCfg blockCfg, StoreConfiguration storeCfgDel) : base(storeCfgDel)
    {
      blockConfig = blockCfg;
    }

    /// <summary>
    /// Interface for Attribute object blockConfig
    /// </summary>
    public FlsEepBlkCfg BlockConfig { get => blockConfig; set => blockConfig = value; }

    /// <summary>
    /// Interface for Attribute object flsEepBlkName
    /// </summary>
    public string BlockName { get => blockConfig.flsEepBlkName; set => blockConfig.flsEepBlkName = value; }

    /// <summary>
    /// Interface for Attribute object flsEepBlkLength
    /// </summary>
    public UInt16 BlockLength { get => blockConfig.flsEepBlkLength; set => blockConfig.flsEepBlkLength = value; }
  }
}
