﻿/*****************************************************************************************************************************
 * @file        PageCfgVM.cs                                                                                                 *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        26.03.2019                                                                                                   *
 * @brief       Implementation of the Page Configuration View Model                                                          *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/
/**
 * @addtogroup ModuleLibrary.Versions.v_1_0_0.ViewModels
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using ModuleLibrary.Versions.v_1_0_0.Models;
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name Space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace ModuleLibrary.Versions.v_1_0_0.ViewModels
{
  /// <summary>
  /// Data Model for the Page Configuration Data
  /// </summary>
  public class PageCfgVM : BaseViewModel
  {
    private string usedPageName;

    private static Models.FlsEepXml FlsEepXmlCfg = new Models.FlsEepXml();
    FlsEepPageCfg pageCfgData;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="page"></param>
    /// <param name="pageName"> Reference to config data</param>
    public PageCfgVM(FlsEepPageCfg pageCfg, string pageName, StoreConfiguration storeCfgDel) : base(storeCfgDel)
    {
      usedPageName = pageName;
      pageCfgData = pageCfg;
    }

    #region Attributes
    /// <summary>
    /// Interface for the Attribute Page Name
    /// </summary>
    public string PageName
    {
      get => usedPageName;
    }
    /// <summary>
    /// Interface for the Attribute Page Start Address
    /// </summary>
    public string PageStartAddess
    {
      get => pageCfgData.flsEepPageStartAddress;
      set => pageCfgData.flsEepPageStartAddress = value;
    }

    /// <summary>
    /// Interface for the Attribute Page Length
    /// </summary>
    public string PageLength
    {
      get => pageCfgData.flsEepPageLength;
      set => pageCfgData.flsEepPageLength = value;
    }

    /// <summary>
    /// Interface for the Attribute Flash Sector Id
    /// </summary>
    public Byte FlashSectorId
    {
      get => pageCfgData.flsEepFlashSectorId;
      set => pageCfgData.flsEepFlashSectorId = value;
    }

    /// <summary>
    /// Interface for the Attribute pageCfgData
    /// </summary>
    public FlsEepPageCfg PageCfgData { get => pageCfgData; set => pageCfgData = value; }
    #endregion
  }
}
