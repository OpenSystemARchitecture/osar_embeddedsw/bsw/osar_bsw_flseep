/*****************************************************************************************************************************
 * @file        Fls.h                                                                                                        *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        12.08.2018 10:16:01                                                                                          *
 * @brief       Implementation of definitions / interface function prototypes / datatypes and generic module interface       *
 *              informations of the "Fls" module.                                                                            *
 *                                                                                                                           *
 * @details     Implementation of the basic flash driver functionality.,                                                    *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
 * @libNote     OSAR Source File Library v.1.0.0.0                                                                           *
*****************************************************************************************************************************/
#ifndef __FLS_H
#define __FLS_H

/**
 * @addtogroup OSAR_MCAL_STM32F4CUBE 
 * @{
 */
/**
 * @addtogroup Fls
 * @{
 */

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 Start of include area                        << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
#include "Fls_Types.h"
#include "Fls_PBCfg.h"
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                  End of include area                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of public data types                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of public data types                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>            Start of public definition area                   << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of public definition area                    << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                Start of global variables                     << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                 End of global variables                      << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>           Start of public function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
/**
 * @brief           Module global memory initialization function.
 * @param[in]       None
 * @retval          None
 * @details         This function shall be called during system startup. And before calling the init and mainfuntion.
 */
void Fls_InitMemory( void );

/**
 * @brief           Module global initialization function.
 * @param[in]       None
 * @retval          None
 * @details         This function shall be called before using the module and after the memory initialization.
 */
void Fls_Init( void );

/**
 * @brief           Lock the Flash module so no requests would be accepted
 * @param[in]       None
 * @retval          Fls_ReturnType
 *                  > FLS_E_OK
 */
Fls_ReturnType Fls_LockFlash(void);

/**
 * @brief           Unlock the Flash module so requests would be accepted
 * @param[in]       None
 * @retval          Fls_ReturnType
 *                  > FLS_E_OK
 *                  > FLS_E_NOT_OK
 */
Fls_ReturnType Fls_UnLockFlash(void);

/**
 * @brief           Wait for the last operation
 * @param[in]       uint32 Timeout value in ms
 * @retval          Fls_ReturnType
 *                  > FLS_E_OK        <Operation successful
 *                  > FLS_E_NOT_OK    <Operation aboard with an error >> Flash within error state?
 *                  > FLS_E_PENDING   <Operation is still pending
 */
Fls_ReturnType Fls_WaitForLastOperation(uint32 timoutMs);

/**
 * @brief           Request actual flash memory status
 * @param[in]       None
 * @retval          FlsEep_ReturnType
 *                  > FLS_E_OK
 *                  > FLS_E_NOT_OK
 *                  > FLS_E_PENDING
 * @note            Conversion of flash hw status register bits to Fls_ReturnType
 */
Fls_ReturnType Fls_GetFlsMemoryStatus( void );

/**
 * @brief           Trigger a synchronous erase of an complete flash sector
 * @param[in]       uint32 Sector Id which shall be erased to be erased.
 * @retval          Fls_ReturnType
 *                  > FLS_E_OK        <Operation successful
 *                  > FLS_E_NOT_OK    <Operation aboard with an error >> Flash within error state?
 *                  > FLS_E_PENDING   <Operation is still pending
 */
Fls_ReturnType Fls_SyncEraseFlsSector( uint32 sectorId );

/**
 * @brief           Programming synchronous one byte to the flash
 * @param[in]       uint8 Byte data to be programmed
 * @param[in]       uint32 Address the data should be programmed to
 * @retval          Fls_ReturnType
 *                  > FLS_E_OK        <Operation successful
 *                  > FLS_E_NOT_OK    <Operation aboard with an error >> Flash within error state?
 *                  > FLS_E_PENDING   <Operation is still pending
 */
Fls_ReturnType Fls_SyncProgramByte(uint8 data, uint32 address);

/**
 * @brief           Programming synchronous one half word to the flash
 * @param[in]       uint16 half word data to be programmed
 * @param[in]       uint32 Address the data should be programmed to
 * @retval          Fls_ReturnType
 *                  > FLS_E_OK        <Operation successful
 *                  > FLS_E_NOT_OK    <Operation aboard with an error >> Flash within error state?
 *                  > FLS_E_PENDING   <Operation is still pending
 */
Fls_ReturnType Fls_SyncProgramHalfWord(uint16 data, uint32 address);

/**
 * @brief           Programming synchronous one word to the flash
 * @param[in]       uint32 word data to be programmed
 * @param[in]       uint32 Address the data should be programmed to
 * @retval          Fls_ReturnType
 *                  > FLS_E_OK        <Operation successful
 *                  > FLS_E_NOT_OK    <Operation aboard with an error >> Flash within error state?
 *                  > FLS_E_PENDING   <Operation is still pending
 */
Fls_ReturnType Fls_SyncProgramWord(uint32 data, uint32 address);

/**
 * @brief           Programming synchronous one double word to the flash
 * @param[in]       uint64 double word data to be programmed
 * @param[in]       uint32 Address the data should be programmed to
 * @retval          Fls_ReturnType
 *                  > FLS_E_OK        <Operation successful
 *                  > FLS_E_NOT_OK    <Operation aboard with an error >> Flash within error state?
 *                  > FLS_E_PENDING   <Operation is still pending
 */
Fls_ReturnType Fls_SyncProgramDoubleWord(uint64 data, uint32 address);

/**
 * @brief           Trigger a asynchronous erase of an complete flash sector
 * @param[in]       uint32 Sector Id which shall be erased to be erased.
 * @retval          Fls_ReturnType
 *                  > FLS_E_OK        <Operation successful
 *                  > FLS_E_NOT_OK    <Operation aboard with an error >> Flash within error state?
 */
Fls_ReturnType Fls_AsyncEraseFlsSector( uint32 sectorId );

/**
 * @brief           Programming asynchronous one byte to the flash
 * @param[in]       uint8 Byte data to be programmed
 * @param[in]       uint32 Address the data should be programmed to
 * @retval          Fls_ReturnType
 *                  > FLS_E_OK        <Operation successful
 *                  > FLS_E_NOT_OK    <Operation aboard with an error >> Flash within error state?
 */
Fls_ReturnType Fls_AsyncProgramByte(uint8 data, uint32 address);

/**
 * @brief           Programming asynchronous one half word to the flash
 * @param[in]       uint16 half word data to be programmed
 * @param[in]       uint32 Address the data should be programmed to
 * @retval          Fls_ReturnType
 *                  > FLS_E_OK        <Operation successful
 *                  > FLS_E_NOT_OK    <Operation aboard with an error >> Flash within error state?
 */
Fls_ReturnType Fls_AsyncProgramHalfWord(uint16 data, uint32 address);

/**
 * @brief           Programming asynchronous one word to the flash
 * @param[in]       uint32 word data to be programmed
 * @param[in]       uint32 Address the data should be programmed to
 * @retval          Fls_ReturnType
 *                  > FLS_E_OK        <Operation successful
 *                  > FLS_E_NOT_OK    <Operation aboard with an error >> Flash within error state?
 */
Fls_ReturnType Fls_AsyncProgramWord(uint32 data, uint32 address);

/**
 * @brief           Programming asynchronous one double word to the flash
 * @param[in]       uint64 double word data to be programmed
 * @param[in]       uint32 Address the data should be programmed to
 * @retval          Fls_ReturnType
 *                  > FLS_E_OK        <Operation successful
 *                  > FLS_E_NOT_OK    <Operation aboard with an error >> Flash within error state?
 */
Fls_ReturnType Fls_AsyncProgramDoubleWord(uint64 data, uint32 address);

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>             End of public function prototypes                << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/

/**
 * @}
 */
/**
 * @}
 */

#endif /* __FLS_H*/
