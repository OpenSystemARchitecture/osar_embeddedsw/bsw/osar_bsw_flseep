echo off
echo Start generation of OSAR Modules:

echo -----------------------------------------------------------------------------------------------------------
echo ---------------------------------------- Generation of BSW Modules ----------------------------------------
echo -----------------------------------------------------------------------------------------------------------

echo ------------------------------------------- Generate Det modul: -------------------------------------------
set "basePath=%~dp0.\..\BSW\Det\"
set "cfgFilePath=.\01_Generator\DetModuleCfg.xml"
set "libFilePath=.\01_Generator\DetModuleLibrary.dll"

set cfgFilePathStr=%basePath%%cfgFilePath%
set libFilePathStr=%basePath%%libFilePath%

OsarStandaloneModuleGenerator.exe -m %basePath% -c %cfgFilePathStr% -l %libFilePathStr% -g

echo ------------------------------------------- Generate Crc modul: -------------------------------------------
set "basePath=%~dp0.\..\BSW\Crc\"
set "cfgFilePath=.\01_Generator\CrcModuleCfg.xml"
set "libFilePath=.\01_Generator\CrcModuleLibrary.dll"

set cfgFilePathStr=%basePath%%cfgFilePath%
set libFilePathStr=%basePath%%libFilePath%

OsarStandaloneModuleGenerator.exe -m %basePath% -c %cfgFilePathStr% -l %libFilePathStr% -g

echo ----------------------------------------- Generate FlsEep modul: ------------------------------------------
set "basePath=%~dp0.\..\..\02_Software\"
set "cfgFilePath=.\01_Generator\FlsEepModuleCfg.xml"
set "libFilePath=.\01_Generator\FlsEepModuleLibrary.dll"

set cfgFilePathStr=%basePath%%cfgFilePath%
set libFilePathStr=%basePath%%libFilePath%

OsarStandaloneModuleGenerator.exe -m %basePath% -c %cfgFilePathStr% -l %libFilePathStr% -g



echo -----------------------------------------------------------------------------------------------------------
echo --------------------------------------- Generation of MCAL Modules ----------------------------------------
echo -----------------------------------------------------------------------------------------------------------

echo ------------------------------------------- Generate Fls modul: -------------------------------------------
set "basePath=%~dp0.\..\MCAL\Fls\"
set "cfgFilePath=.\01_Generator\FlsModuleCfg.xml"
set "libFilePath=.\01_Generator\FlsModuleLibrary.dll"

set cfgFilePathStr=%basePath%%cfgFilePath%
set libFilePathStr=%basePath%%libFilePath%

OsarStandaloneModuleGenerator.exe -m %basePath% -c %cfgFilePathStr% -l %libFilePathStr% -g
